package com.epf.rentmanager.service;

import java.util.List;
import java.sql.Date;

import com.epf.rentmanager.model.Reservation;
import com.epf.rentmanager.dao.ReservationDao;

import com.epf.rentmanager.exception.DaoException;
import com.epf.rentmanager.exception.ServiceException;

public class ReservationService {

	private ReservationDao reservationDao;
	public static ReservationService instance;

	private ReservationService() {
		this.reservationDao = ReservationDao.getInstance();
	}

	public static ReservationService getInstance() {
		if (instance == null) {
			instance = new ReservationService();
		}

		return instance;
	}

	public List<Reservation> findAll() throws ServiceException {
		try {
			return reservationDao.findAll();
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int countReservation() throws ServiceException {
		try {
			return reservationDao.countReservation();
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int addReservation(int client_id, int vehicle_id, Date debut, Date fin) throws ServiceException {
		try {
			return reservationDao.addReservation(client_id, vehicle_id, debut, fin);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int deleteReservation(int id_reservation) throws ServiceException {
		try {
			return reservationDao.deleteReservation(id_reservation);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public Reservation findReservation(int id_reservation) throws ServiceException {
		try {
			return reservationDao.findReservation(id_reservation);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int UpdateReservation(int client_id, int vehicle_id, Date debut, Date fin, int id) throws ServiceException {

		try {
			return reservationDao.UpdateReservation(client_id, vehicle_id, debut, fin, id);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

}
