package com.epf.rentmanager.service;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.sql.Date;

import com.epf.rentmanager.exception.DaoException;
import com.epf.rentmanager.exception.ServiceException;

import com.epf.rentmanager.model.Reservation;
//import com.epf.rentmanager.service.ReservationService;

import com.epf.rentmanager.dao.ClientDao;
import com.epf.rentmanager.model.Client;

public class ClientService {

	List<Reservation> reservations;

	public static ClientService instance;
	private ClientDao clientDao;

	private ClientService() {
		this.clientDao = ClientDao.getInstance();
	}

	public static ClientService getInstance() {
		if (instance == null) {
			instance = new ClientService();
		}

		return instance;
	}

	public List<Client> findAll() throws ServiceException {
		try {
			return clientDao.findAll();
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int countClient() throws ServiceException {
		try {
			return clientDao.countClient();
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int addClient(String nom, String prenom, String email, Date naissance) throws ServiceException {
		if (nom.length() < 3) {
			throw new ServiceException("Le nom doit faire au moins 3 caractères");
		}
		if (prenom.length() < 3) {
			throw new ServiceException("Le prénom doit faire au moins 3 caractères");
		}
		if (email.length() == 0) {
			throw new ServiceException("Veuillez rentrer une adresse mail valide");
		}
		try {
			List<Client> clients = clientDao.findAll();
			boolean email_used = false;
			for (Client client : clients) {
				if (client.getEmail().equals(email)) {
					email_used = true;
				}
			}
			if (email_used) {
				throw new ServiceException("Un compte est déjà associé à cette adresse email");
			}
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}

		long millis = System.currentTimeMillis();
		java.sql.Date now = new java.sql.Date(millis);

		long diffInMillies = Math.abs(now.getTime() - naissance.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		System.out.println(diff);
		long age = diff / 365;
		if (age < 18) {
			throw new ServiceException("Il faut avoir minimum 18 ans pour s'inscrire");
		}

		try {
			return clientDao.addClient(nom, prenom, email, naissance);
		} catch (DaoException e1) {
			throw new ServiceException(e1.getMessage());
		}
	}

	public int deleteClient(int id_client) throws ServiceException {
		try {
			reservations = ReservationService.getInstance().findAll();
			for (Reservation reservation : reservations) {
				if (reservation.getClient_id() == id_client) {
					ReservationService.getInstance().deleteReservation(reservation.getId());
				}
			}
			return clientDao.deleteClient(id_client);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public Client findClient(int id_client) throws ServiceException {
		try {
			return clientDao.findClient(id_client);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int updateClient(String nom, String prenom, String email, Date naissance, int id) throws ServiceException {
		if (nom.length() < 3) {
			throw new ServiceException("Le nom doit faire au moins 3 caractères");
		}
		if (prenom.length() < 3) {
			throw new ServiceException("Le prénom doit faire au moins 3 caractères");
		}
		if (email.length() == 0) {
			throw new ServiceException("Veuillez rentrer une adresse mail valide");
		}
		try {
			List<Client> clients = clientDao.findAll();
			boolean email_used = false;
			for (Client client : clients) {
				if (client.getEmail().equals(email) && client.getId() != id) {
					email_used = true;
				}
			}
			if (email_used) {
				throw new ServiceException("Un compte est déjà associé à cette adresse email");
			}
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}

		long millis = System.currentTimeMillis();
		java.sql.Date now = new java.sql.Date(millis);

		long diffInMillies = Math.abs(now.getTime() - naissance.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		System.out.println(diff);
		long age = diff / 365;
		if (age < 18) {
			throw new ServiceException("Il faut avoir minimum 18 ans pour s'inscrire");
		}

		try {
			return clientDao.updateClient(nom, prenom, email, naissance, id);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int countClientReservations(int id) throws ServiceException {
		try {
			return clientDao.countClientReservations(id);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

}
