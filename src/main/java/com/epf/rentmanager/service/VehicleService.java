package com.epf.rentmanager.service;

import java.util.List;

import com.epf.rentmanager.exception.DaoException;
import com.epf.rentmanager.exception.ServiceException;

import com.epf.rentmanager.model.Vehicle;
import com.epf.rentmanager.dao.VehicleDao;

import com.epf.rentmanager.model.Reservation;

public class VehicleService {

	List<Reservation> reservations;

	public static VehicleService instance;
	private VehicleDao vehicleDao;

	private VehicleService() {
		this.vehicleDao = VehicleDao.getInstance();
	}

	public static VehicleService getInstance() {
		if (instance == null) {
			instance = new VehicleService();
		}

		return instance;
	}

	public List<Vehicle> findAll() throws ServiceException {
		try {
			return vehicleDao.findAll();
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int countVehicle() throws ServiceException {
		try {
			return vehicleDao.countVehicle();
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int addVehicle(String constructeur, String modele, int nb_places) throws ServiceException {
		if (nb_places < 2 || nb_places > 9) {
			throw new ServiceException("Une voiture ne peut avoir qu'entre 2 et 9 places");
		}
		try {
			return vehicleDao.addVehicle(constructeur, modele, nb_places);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int deleteVehicle(int id_vehicle) throws ServiceException {
		try {
			reservations = ReservationService.getInstance().findAll();
			for (Reservation reservation : reservations) {
				if (reservation.getVehicle_id() == id_vehicle) {
					ReservationService.getInstance().deleteReservation(reservation.getId());
				}
			}
			return vehicleDao.deleteVehicle(id_vehicle);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public Vehicle findVehicle(int id_vehicle) throws ServiceException {
		try {
			return vehicleDao.findVehicle(id_vehicle);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int UpdateVehicle(String constructeur, String modele, int nb_places, int id) throws ServiceException {
		if (nb_places < 2 || nb_places > 9) {
			throw new ServiceException("Une voiture ne peut avoir qu'entre 2 et 9 places");
		}
		try {
			return vehicleDao.UpdateVehicle(constructeur, modele, nb_places, id);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}

	public int countVehicleReservations(int id) throws ServiceException {
		try {
			return vehicleDao.countVehicleReservations(id);
		} catch (DaoException e) {
			throw new ServiceException();
		}
	}
}
