package com.epf.rentmanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.epf.rentmanager.exception.DaoException;
import com.epf.rentmanager.model.Vehicle;
import com.epf.rentmanager.persistence.ConnectionManager;

public class VehicleDao {
	
	private static VehicleDao instance = null;
	private VehicleDao() {}
	public static VehicleDao getInstance() {
		if(instance == null) {
			instance = new VehicleDao();
		}
		return instance;
	}
	
	private static final String CREATE_VEHICLE_QUERY = "INSERT INTO Vehicle(constructeur, modele, nb_places) VALUES(?, ?, ?);";
	private static final String DELETE_VEHICLE_QUERY = "DELETE FROM Vehicle WHERE id=?;";
	private static final String FIND_VEHICLE_QUERY = "SELECT id, constructeur, modele, nb_places FROM Vehicle WHERE id = ?;";
	private static final String FIND_VEHICLES_QUERY = "SELECT id, constructeur, modele, nb_places FROM Vehicle;";
	private static final String COUNT_VEHICLES_QUERY = "SELECT COUNT(id) as total_vehicle FROM Vehicle;";
	private static final String UPDATE_VEHICLE_QUERY = "UPDATE Vehicle SET constructeur = ?, modele = ?, nb_places = ? WHERE id = ?;";
	private static final String COUNT_VEHICLE_RESERVATIONS_QUERY = "SELECT COUNT(id) as total_vehicle_reservations FROM Reservation WHERE vehicle_id = ?";
	
	/**
	 * 
	 * @return 
	 * @throws DaoException 
	 */
	public List<Vehicle> findAll() throws DaoException {
		List<Vehicle> result = new ArrayList<>();
		try (Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(FIND_VEHICLES_QUERY)) {
												
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				Vehicle vehicle = new Vehicle();
				vehicle.setId(resultSet.getInt("id"));
				vehicle.setConstructeur(resultSet.getString("constructeur"));
				vehicle.setModele(resultSet.getString("modele"));			
				vehicle.setNb_places(resultSet.getByte("nb_places"));
				result.add(vehicle);
			}
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}
		return result;
	}


	public int countVehicle() throws DaoException{
		int result;
		result = 0;
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(COUNT_VEHICLES_QUERY); ) {
		

		ResultSet resultSet = preparedStatement.executeQuery();

		if(resultSet.next()){
			result = resultSet.getInt("total_vehicle");
		}
			resultSet.close();
	} catch (SQLException e) {
		e.printStackTrace();
		throw new DaoException();
	}
		return result;
	}

	public int addVehicle(String constructeur, String modele, int nb_places) throws DaoException{
		try {
		Connection connection = ConnectionManager.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(CREATE_VEHICLE_QUERY, Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, constructeur);
		preparedStatement.setString(2, modele);
		preparedStatement.setInt(3, nb_places);

		preparedStatement.executeUpdate();

		ResultSet resultSet = preparedStatement.getGeneratedKeys();
		if(resultSet.next()){
			return resultSet.getInt(1);
		}
		resultSet.close();
	} catch (SQLException e) {
		throw new DaoException();
	}
		return 1;
	}

	public int deleteVehicle(int id_vehicle) throws DaoException {		
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(DELETE_VEHICLE_QUERY); ) {									
			preparedStatement.setInt(1, id_vehicle);			
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException();
		}	
	}

	public Vehicle findVehicle(int id_vehicle) throws DaoException {
		Vehicle vehicle = new Vehicle();	
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(FIND_VEHICLE_QUERY); ) {									
			
			preparedStatement.setInt(1, id_vehicle);						
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				vehicle.setId(resultSet.getInt("id"));
				vehicle.setConstructeur(resultSet.getString("constructeur"));
				vehicle.setModele(resultSet.getString("modele"));
				vehicle.setNb_places(resultSet.getInt("nb_places"));
			}
			resultSet.close();

			
		} catch (SQLException e) {
			throw new DaoException();
		}	
		return vehicle;
	}

	public int UpdateVehicle(String constructeur, String modele, int nb_places, int id) throws DaoException {		
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_VEHICLE_QUERY); ) {									
			preparedStatement.setString(1, constructeur);
			preparedStatement.setString(2, modele);
			preparedStatement.setInt(3, nb_places);
			preparedStatement.setInt(4, id);
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException();
		}	
	}

	public int countVehicleReservations(int id) throws DaoException{
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(COUNT_VEHICLE_RESERVATIONS_QUERY); ) {									
			int count = 0;
			preparedStatement.setInt(1, id);	
			ResultSet resultSet = preparedStatement.executeQuery();	
			if (resultSet.next()){
				count = resultSet.getInt("total_vehicle_reservations");
			}
			resultSet.close();
			return count;
		} catch (SQLException e) {
			throw new DaoException();
		}	
	}

}