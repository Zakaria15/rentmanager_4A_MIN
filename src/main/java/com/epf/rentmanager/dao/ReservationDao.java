package com.epf.rentmanager.dao;

import com.epf.rentmanager.exception.DaoException;
import com.epf.rentmanager.model.Reservation;
import com.epf.rentmanager.persistence.ConnectionManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class ReservationDao {
	private static ReservationDao instance = null;

	private ReservationDao() {
	}

	public static ReservationDao getInstance() {
		if (instance == null) {
			instance = new ReservationDao();
		}
		return instance;
	}

	private static final String CREATE_RESERVATION_QUERY = "INSERT INTO Reservation(client_id, vehicle_id, debut, fin) VALUES(?, ?, ?, ?);";
	private static final String DELETE_RESERVATION_QUERY = "DELETE FROM Reservation WHERE id=?;";
	private static final String FIND_RESERVATION_QUERY = "SELECT id, client_id, vehicle_id, debut, fin FROM Reservation WHERE id=?;";
	private static final String FIND_RESERVATIONS_QUERY = "SELECT id, client_id, vehicle_id, debut, fin FROM Reservation;";
	private static final String COUNT_RESERVATIONS_QUERY = "SELECT COUNT(id) as total_reservation FROM Reservation;";
	private static final String UPDATE_RESERVATION_QUERY = "UPDATE Reservation SET client_id = ?, vehicle_id = ?, debut = ?, fin = ? WHERE id = ?;";

	/**
	 * 
	 * @return
	 * @throws DaoException
	 */
	public List<Reservation> findAll() throws DaoException {
		List<Reservation> result = new ArrayList<>();
		try (Connection connection = ConnectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(FIND_RESERVATIONS_QUERY);) {

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Reservation reservation = new Reservation();
				reservation.setId(resultSet.getInt("id"));
				reservation.setClient_id(resultSet.getInt("client_id"));
				reservation.setVehicle_id(resultSet.getInt("vehicle_id"));
				reservation.setDebut(resultSet.getDate("debut"));
				reservation.setFin(resultSet.getDate("fin"));
				result.add(reservation);
			}
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}
		return result;
	}

	public int countReservation() throws DaoException {
		int result;
		result = 0;
		try (Connection connection = ConnectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(COUNT_RESERVATIONS_QUERY);) {

			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				result = resultSet.getInt("total_reservation");
			}
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}
		return result;
	}

	public int addReservation(int client_id, int vehicle_id, Date debut, Date fin) throws DaoException {
		try {
			Connection connection = ConnectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(CREATE_RESERVATION_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, client_id);
			preparedStatement.setInt(2, vehicle_id);
			preparedStatement.setDate(3, debut);
			preparedStatement.setDate(4, fin);

			preparedStatement.executeUpdate();

			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}
			resultSet.close();
		} catch (SQLException e) {
			throw new DaoException();
		}
		return 1;
	}

	public int deleteReservation(int id_reservation) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_RESERVATION_QUERY);) {
			preparedStatement.setInt(1, id_reservation);
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException();
		}
	}

	public Reservation findReservation(int id_reservation) throws DaoException {
		Reservation reservation = new Reservation();
		try (Connection connection = ConnectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(FIND_RESERVATION_QUERY);) {

			preparedStatement.setInt(1, id_reservation);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				reservation.setId(resultSet.getInt("id"));
				reservation.setClient_id(resultSet.getInt("client_id"));
				reservation.setVehicle_id(resultSet.getInt("vehicle_id"));
				reservation.setDebut(resultSet.getDate("debut"));
				reservation.setFin(resultSet.getDate("fin"));
			}
			resultSet.close();

		} catch (SQLException e) {
			throw new DaoException();
		}
		return reservation;
	}

	public int UpdateReservation(int client_id, int vehicle_id, Date debut, Date fin, int id) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_RESERVATION_QUERY);) {
			preparedStatement.setInt(1, client_id);
			preparedStatement.setInt(2, vehicle_id);
			preparedStatement.setDate(3, debut);
			preparedStatement.setDate(4, fin);
			preparedStatement.setInt(5, id);
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException();
		}
	}

}
