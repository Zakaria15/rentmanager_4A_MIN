package com.epf.rentmanager.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.epf.rentmanager.exception.DaoException;
import com.epf.rentmanager.model.Client;
import com.epf.rentmanager.persistence.ConnectionManager;

public class ClientDao {
	
	private static ClientDao instance = null;
	private ClientDao() {}
	public static ClientDao getInstance() {
		if(instance == null) {
			instance = new ClientDao();
		}
		return instance;
	}
	
	private static final String CREATE_CLIENT_QUERY = "INSERT INTO Client(nom, prenom, email, naissance) VALUES(?, ?, ?, ?);";
	private static final String DELETE_CLIENT_QUERY = "DELETE FROM Client WHERE id=?;";
	private static final String FIND_CLIENT_QUERY = "SELECT id, nom, prenom, email, naissance FROM Client WHERE id=?;";
	private static final String FIND_CLIENTS_QUERY = "SELECT id, nom, prenom, email, naissance FROM Client;";
	private static final String COUNT_CLIENTS_QUERY = "SELECT COUNT(id) as total_client FROM Client;";
	private static final String UPDATE_CLIENT_QUERY = "UPDATE Client SET nom = ?, prenom = ?, email = ?, naissance = ? WHERE id = ?;";
	private static final String COUNT_CLIENT_RESERVATIONS_QUERY = "SELECT COUNT(id) as total_client_reservations FROM Reservation WHERE client_id = ?";

	/**
	 * 
	 * @return 
	 * @throws DaoException 
	 */
	public List<Client> findAll() throws DaoException {
		List<Client> result = new ArrayList<>();
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(FIND_CLIENTS_QUERY); ) {
						
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				Client client = new Client();
				client.setId(resultSet.getInt("id"));
				client.setNom(resultSet.getString("nom"));
				client.setPrenom(resultSet.getString("prenom"));
				client.setEmail(resultSet.getString("email"));
				client.setNaissance(resultSet.getDate("naissance"));
				result.add(client);
			}
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}
		return result;
	}

	public int countClient() throws DaoException{
		int result;
		result = 0;
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(COUNT_CLIENTS_QUERY); ) {
		

		ResultSet resultSet = preparedStatement.executeQuery();

		if(resultSet.next()){
			result = resultSet.getInt("total_client");
		}
			resultSet.close();
	} catch (SQLException e) {
		e.printStackTrace();
		throw new DaoException();
	}
		return result;
	}

	public int addClient(String nom, String prenom, String email, Date naissance) throws DaoException{
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(CREATE_CLIENT_QUERY, Statement.RETURN_GENERATED_KEYS); ) {
		
		
		preparedStatement.setString(1, nom);
		preparedStatement.setString(2, prenom);
		preparedStatement.setString(3, email);
		preparedStatement.setDate(4, naissance);

		preparedStatement.executeUpdate();

		ResultSet resultSet = preparedStatement.getGeneratedKeys();
		if(resultSet.next()){
			return resultSet.getInt(1);
		}
		resultSet.close();
	} catch (SQLException e) {
		throw new DaoException();
	}
		return 1;
	}

	public Client findClient(int id_client) throws DaoException {
		Client client = new Client();	
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(FIND_CLIENT_QUERY); ) {									
				
			preparedStatement.setInt(1, id_client);						
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				client.setId(resultSet.getInt("id"));
				client.setNom(resultSet.getString("nom"));
				client.setPrenom(resultSet.getString("prenom"));
				client.setEmail(resultSet.getString("email"));
				client.setNaissance(resultSet.getDate("naissance"));
			}
			resultSet.close();
		
		} catch (SQLException e) {
			throw new DaoException();
		}	
		return client;
	}

	public int deleteClient(int id_client) throws DaoException {		
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(DELETE_CLIENT_QUERY); ) {									
			preparedStatement.setInt(1, id_client);			
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException();
		}	
	}

	public int updateClient(String nom, String prenom, String email, Date naissance, int id) throws DaoException {		
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CLIENT_QUERY); ) {									
			preparedStatement.setString(1, nom);
			preparedStatement.setString(2, prenom);
			preparedStatement.setString(3, email);
			preparedStatement.setDate(4, naissance);
			preparedStatement.setInt(5, id);		
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException();
		}	
	}

	public int countClientReservations(int id) throws DaoException{
		try(Connection connection = ConnectionManager.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(COUNT_CLIENT_RESERVATIONS_QUERY); ) {									
			int count = 0;
			preparedStatement.setInt(1, id);	
			ResultSet resultSet = preparedStatement.executeQuery();	
			if (resultSet.next()){
				count = resultSet.getInt("total_client_reservations");
			}
			resultSet.close();
			return count;
		} catch (SQLException e) {
			throw new DaoException();
		}	
	}
}
