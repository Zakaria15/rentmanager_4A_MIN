package com.epf.rentmanager.ui;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.ClientService;

@WebServlet("/users/create")
public class ClientAddServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/create.jsp");
        dispatcher.forward(request, response);

    }
   
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {

            String nom, prenom, email;
            Date naissance;
            ClientService clientService = ClientService.getInstance();

            nom = request.getParameter("nom");
            prenom = request.getParameter("prenom");
            email = request.getParameter("email");
            naissance = Date.valueOf(request.getParameter("naissance"));

            clientService.addClient(nom, prenom, email, naissance);
            
            response.sendRedirect("http://localhost:8080/rentmanager/users");

        } catch (ServiceException e) {

            String nom, prenom, email;
            Date naissance;

            nom = request.getParameter("nom");
            prenom = request.getParameter("prenom");
            email = request.getParameter("email");
            naissance = Date.valueOf(request.getParameter("naissance"));

            request.setAttribute("nom", nom);
            request.setAttribute("prenom", prenom);
            request.setAttribute("email", email);
            request.setAttribute("naissance", naissance);

            String error = e.getMessage();
            request.setAttribute("error", error);

            doGet(request, response);

        }            
    }
}