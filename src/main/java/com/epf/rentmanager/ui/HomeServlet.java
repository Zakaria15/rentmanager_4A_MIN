package com.epf.rentmanager.ui;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.ClientService;
import com.epf.rentmanager.service.ReservationService;
import com.epf.rentmanager.service.VehicleService;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        try {

            ClientService clientService = ClientService.getInstance();
            ReservationService reservationService = ReservationService.getInstance();
            VehicleService vehicleService = VehicleService.getInstance();

            request.setAttribute("total_client", clientService.countClient());
            request.setAttribute("total_reservation", reservationService.countReservation());
            request.setAttribute("total_vehicle", vehicleService.countVehicle());

            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/home.jsp");
            dispatcher.forward(request, response);

        } catch (ServiceException e) {
            
            e.printStackTrace();

        }             
    }
}
