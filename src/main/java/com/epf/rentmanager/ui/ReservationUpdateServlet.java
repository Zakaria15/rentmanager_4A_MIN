package com.epf.rentmanager.ui;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.ReservationService;
import com.epf.rentmanager.service.ClientService;
import com.epf.rentmanager.service.VehicleService;

@WebServlet("/rents/update")
public class ReservationUpdateServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        try {

            int id;
            ReservationService reservationService = ReservationService.getInstance();
            ClientService clientService = ClientService.getInstance();
            VehicleService vehicleService = VehicleService.getInstance();
               
            id = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("reservation", reservationService.findReservation(id));
            request.setAttribute("clients", clientService.findAll());
            request.setAttribute("vehicles", vehicleService.findAll());

            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/update.jsp");
            dispatcher.forward(request, response);   

        } catch (ServiceException e) {
            
            e.printStackTrace();

        }
    }
   
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
                                              
            int id, car, client;
            Date begin, end;
            ReservationService reservationService = ReservationService.getInstance();
                    
            id = Integer.parseInt(request.getParameter("id"));
            car = Integer.valueOf(request.getParameter("car"));
            client = Integer.valueOf(request.getParameter("client"));
            begin = Date.valueOf(request.getParameter("begin"));
            end = Date.valueOf(request.getParameter("end"));

            reservationService.UpdateReservation(client, car, begin, end, id);

            doGet(request, response);

        } catch (ServiceException e) {
                    
            String error = e.getMessage();
            request.setAttribute("error", error);
            
            doGet(request, response);

        }
    }
}
