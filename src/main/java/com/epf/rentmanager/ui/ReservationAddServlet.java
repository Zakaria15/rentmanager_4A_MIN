package com.epf.rentmanager.ui;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.ReservationService;
import com.epf.rentmanager.service.VehicleService;
import com.epf.rentmanager.service.ClientService;

@WebServlet("/rents/create")
public class ReservationAddServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       

        try {

            ClientService clientService = ClientService.getInstance();
            VehicleService vehicleService = VehicleService.getInstance();

            request.setAttribute("clients", clientService.findAll());
            request.setAttribute("vehicles", vehicleService.findAll());

            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/create.jsp");
            dispatcher.forward(request, response);

        } catch (ServiceException e) {
            e.printStackTrace();
        }

        
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            
            int client_id, vehicle_id;
            Date debut, fin;
            ReservationService reservationService = ReservationService.getInstance();

            client_id = Integer.parseInt(request.getParameter("client"));
            vehicle_id = Integer.parseInt(request.getParameter("car"));
            debut = Date.valueOf(request.getParameter("begin"));
            fin = Date.valueOf(request.getParameter("end"));

            reservationService.addReservation(client_id, vehicle_id, debut, fin);

            response.sendRedirect("http://localhost:8080/rentmanager/rents");

        } catch (ServiceException e) {

            String error = e.getMessage();
            request.setAttribute("error", error);

            doGet(request, response);

        }
    }
}
