package com.epf.rentmanager.ui;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.VehicleService;

@WebServlet("/cars/create")
public class VehicleAddServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/vehicles/create.jsp");
        dispatcher.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        try {

            String constructeur, modele;
            int nb_places;
            VehicleService vehicleService = VehicleService.getInstance();

            constructeur = request.getParameter("manufacturer");
            modele = request.getParameter("modele");
            nb_places = Integer.parseInt(request.getParameter("seats"));

            vehicleService.addVehicle(constructeur, modele, nb_places);

            response.sendRedirect("http://localhost:8080/rentmanager/cars");

        } catch (ServiceException e) {

            String constructeur, modele;
            int nb_places;

            constructeur = request.getParameter("manufacturer");
            modele = request.getParameter("modele");
            nb_places = Integer.parseInt(request.getParameter("seats"));

            request.setAttribute("constructeur", constructeur);
            request.setAttribute("modele", modele);
            request.setAttribute("nb_places", nb_places);

            String error = e.getMessage();
            request.setAttribute("error", error);

            doGet(request, response);

        }             
    }  
}
