package com.epf.rentmanager.ui;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.ClientService;

@WebServlet("/users/update")
public class ClientUpdateServlet extends HttpServlet {   

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        try {

            int id;
            ClientService clientService = ClientService.getInstance();
                       
            id = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("client", clientService.findClient(id));

            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/update.jsp");
            dispatcher.forward(request, response);       

        } catch (ServiceException e) {
            
            e.printStackTrace();

        }
    }
   
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
                    
            int id;
            String nom, prenom, email;
            Date naissance;
            ClientService clientService = ClientService.getInstance();

            id = Integer.parseInt(request.getParameter("id"));
            nom = request.getParameter("nom");
            prenom = request.getParameter("prenom");
            email = request.getParameter("email");
            naissance = Date.valueOf(request.getParameter("naissance"));

            clientService.updateClient(nom, prenom, email, naissance, id);

            doGet(request, response); 

        } catch (ServiceException e) {

            String error = e.getMessage();
            request.setAttribute("error", error);

            doGet(request, response);

        }
    }   
}
