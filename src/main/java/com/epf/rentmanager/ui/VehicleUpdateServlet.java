package com.epf.rentmanager.ui;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.VehicleService;

@WebServlet("/cars/update")
public class VehicleUpdateServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        try {

            int id; 
            VehicleService vehicleService = VehicleService.getInstance();         
              
            id = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("vehicle", vehicleService.findVehicle(id));

            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/vehicles/update.jsp");
            dispatcher.forward(request, response);

        } catch (ServiceException e) {
            
            e.printStackTrace();

        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        try {          
           
            int id, nb_places;
            String constructeur, modele;
            VehicleService vehicleService = VehicleService.getInstance();     
            
            id = Integer.parseInt(request.getParameter("id"));
            constructeur = request.getParameter("manufacturer");
            modele = request.getParameter("modele");
            nb_places = Integer.valueOf(request.getParameter("seats"));

            vehicleService.UpdateVehicle(constructeur, modele, nb_places, id);

            doGet(request, response);

        } catch (ServiceException e) {

            String error = e.getMessage();
            request.setAttribute("error", error);

            doGet(request, response);

        }
    }  
}
