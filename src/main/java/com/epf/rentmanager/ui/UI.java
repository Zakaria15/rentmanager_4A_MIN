package com.epf.rentmanager.ui;

import java.util.List;
import java.sql.Date;
import java.time.LocalDate;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.utils.IOUtils;

import com.epf.rentmanager.model.Client;
import com.epf.rentmanager.model.Vehicle;
import com.epf.rentmanager.model.Reservation;

import com.epf.rentmanager.service.ClientService;
import com.epf.rentmanager.service.VehicleService;
import com.epf.rentmanager.service.ReservationService;

public class UI {

    public static void main(String[] args) throws ServiceException {
 
        String menu =   "\nMENU :\nQue souhaitez-vous faire ?\n"
                        + "1 : Afficher la liste des clients\n"
                        + "2 : Ajouter un client\n"
                        + "3 : Afficher les informations d'un client\n"
                        + "4 : Modifier les informations d'un client\n"
                        + "5 : Supprimer un client\n"
                        + "6 : Quitter";
     
        int choix = 0;
        int id_client;      
        int result;
        Client client_found;
        List<Client> clients; 
        
        while(choix != 6){
            choix = IOUtils.readInt(menu);              
    
            switch (choix){

                case 1 : 
                    System.out.println("\nVoici la liste actuelle des clients : ");
                    clients = ClientService.getInstance().findAll();
                    for (Client client : clients){
                        System.out.println(client);
                    }

                break;

                case 2 : 
                   String nom = IOUtils.readString("Nom : ", true);
                   String prenom = IOUtils.readString("Prenom : ", true);
                   String email = IOUtils.readString("Email : ", true);
                   Date naissance = Date.valueOf(IOUtils.readDate("Date : ", true));
                   result = ClientService.getInstance().addClient(nom, prenom, email, naissance);
                   System.out.println("\nLe client n° " + result + " vient d'être créé");

                break;

                case 3 :    
                              
                    id_client = IOUtils.readInt("\nQuel est l'id du client dont il faut afficher les informations ?");
	                client_found = ClientService.getInstance().findClient(id_client);
                    System.out.println(client_found);                       

                break;

                case 4 :
                    System.out.println("\nVeuillez consulter notre site web pour accèder à cette fonctionnalité.");
                break;

                case 5 :
                    id_client = IOUtils.readInt("\nQuel est l'id du client qui doit être supprimé ?");     
                    result = ClientService.getInstance().deleteClient(id_client);
                    boolean client_supprime = result == 1 ? true : false;
                    if(client_supprime){
                        System.out.println("\nLe client n°" + id_client + " a été supprimé avec succès");
                    }else{
                        System.out.println("\nLe client n°" + id_client + " sélectionné n'existe pas");
                    }        
                break;        

                case 6 :
                    System.out.println("\nA bientot !");
                break;

                default:
                    System.out.println("\nL'action sélectionnée n'existe pas !");
            }      
        }
    }
}