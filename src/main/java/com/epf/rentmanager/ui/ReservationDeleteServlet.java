package com.epf.rentmanager.ui;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.ReservationService;

@WebServlet("/rents/delete")
public class ReservationDeleteServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        try {

            int id;
            ReservationService reservationService = ReservationService.getInstance();

            id = Integer.parseInt(request.getParameter("id"));

            reservationService.deleteReservation(id);
            
            response.sendRedirect("http://localhost:8080/rentmanager/rents");

        } catch (ServiceException e) {
            
            e.printStackTrace();

        }
    }    
}
