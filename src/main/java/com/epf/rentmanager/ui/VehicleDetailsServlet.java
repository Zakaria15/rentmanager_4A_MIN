package com.epf.rentmanager.ui;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.ClientService;
import com.epf.rentmanager.service.ReservationService;
import com.epf.rentmanager.service.VehicleService;

@WebServlet("/cars/details")
public class VehicleDetailsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                          
        try {

            int id;
            ClientService clientService = ClientService.getInstance();
            VehicleService vehicleService = VehicleService.getInstance();
            ReservationService reservationService = ReservationService.getInstance(); 

            
            id = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("vehicle", vehicleService.findVehicle(id));
            request.setAttribute("nbr_reservations", vehicleService.countVehicleReservations(id));
            request.setAttribute("clients", clientService.findAll());
            request.setAttribute("reservations", reservationService.findAll());

            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/vehicles/details.jsp");
            dispatcher.forward(request, response); 

        } catch (ServiceException e) {
            
            e.printStackTrace();

        }      
    }   
}
