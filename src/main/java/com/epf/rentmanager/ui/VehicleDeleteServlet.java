package com.epf.rentmanager.ui;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.rentmanager.exception.ServiceException;
import com.epf.rentmanager.service.VehicleService;

@WebServlet("/cars/delete")
public class VehicleDeleteServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        try {

            int id;
            VehicleService vehicleService = VehicleService.getInstance();

            id = Integer.parseInt(request.getParameter("id"));

            vehicleService.deleteVehicle(id);

            response.sendRedirect("http://localhost:8080/rentmanager/cars");

        } catch (ServiceException e) {
            
            e.printStackTrace();
            
        }
    }   
}
