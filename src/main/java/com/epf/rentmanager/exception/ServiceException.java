package com.epf.rentmanager.exception;

public class ServiceException extends Exception {

    public ServiceException() {
        
    }
    public ServiceException(String msg) {
        super(msg);
    }  

}